<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Product extends Migration
{
    public function up() {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type'           => 'VARCHAR',
                'constraint'     => 50,
            ],
            'sell_price' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE
            ],
            'buy_price' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE
            ],
            'stock' => [
                'type'           => 'VARCHAR',
                'constraint'     => 10
            ],
            'img_path' => [
                'type'           => 'VARCHAR',
                'constraint'     => 255
            ],
            'slug' => [
                'type'           => 'VARCHAR',
                'constraint'     => 128
            ],
            'created' => [
                'type'           => 'DATETIME',
            ],
            'updated' => [
                'type'           => 'DATETIME'
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->addKey('name');
        $this->forge->createTable('product');
    }

    //--------------------------------------------------------------------

    public function down() {
        $this->forge->dropTable('product');
    }
}
