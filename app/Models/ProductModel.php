<?php namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model {
    protected $table = 'product';

    protected $allowedFields = [
        'name',
        'sell_price',
        'buy_price',
        'stock',
        'slug',
        'img_path',
        'created',
        'updated'
    ];

    function __construct() {
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table($this->table);
    }

    /**
     * Fetch data
     *
     * @param string $slug
     * @return array
     */
    public function getProducts(string $slug = null) {
        if ($slug === null) {
            return $this->builder->get()->getResultArray();
        } elseif ($slug === 'latest') {
            return $this->builder->orderBy('updated')
                                 ->get(2)
                                 ->getResultArray();
        }
        return $this->builder->getWhere(['slug' => $slug])
                            ->getFirstRow('array');
    }

    /**
     * Update data
     *
     * @param array $data
     * @return boolean
     */
    public function updateProduct(array $data) {
        $this->builder->where('id', $data['id']);
        $query = $this->builder->update($data);

        return ($query ? true : false);
    }
}