<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?= $this->include('backend/templates/_section_header') ?>

  <!-- Main content -->
  <section class="content">
    <?php echo form_open_multipart(
      'backend/product/edit/'.$product['slug'],
      'id="myform"') ?>
      <?= csrf_field() ?>
      <div class="row">
        <div class="col-md-12">
          <!-- Error handler -->
          <?php echo view('errors/_errors_backend_form'); ?>

          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">product</h3>

              <div class="card-tools">
                <button
                  type="button"
                  class="btn btn-tool"
                  data-card-widget="collapse"
                  data-toggle="tooltip"
                  title="Collapse"
                ><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Product Name</label>
                <input
                  type="text"
                  id="inputName"
                  name="name"
                  value="<?php if (!empty($product['name']))
                    echo $product['name'] ?>"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="inputName">Sell Price</label>
                <input
                  type="text"
                  id="inputName"
                  name="sell_price"
                  value="<?php if (!empty($product['sell_price']))
                    echo $product['sell_price'] ?>"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="inputName">Buy Price</label>
                <input
                  type="text"
                  id="inputName"
                  name="buy_price"
                  value="<?php if (!empty($product['buy_price']))
                    echo $product['buy_price'] ?>"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="inputName">Stock</label>
                <input
                  type="text"
                  id="inputName"
                  name="stock"
                  value="<?php if (!empty($product['stock']))
                    echo $product['stock'] ?>"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Image</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input
                      type="file"
                      class="custom-file-input"
                      name="image"
                      id="exampleInputFile"
                    />
                    <label
                      class="custom-file-label"
                      for="exampleInputFile"
                    >Choose file</label>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a
            href="<?php echo base_url('backend/news') ?>"
            class="btn btn-danger"
          >Cancel</a>
          <input
            type="submit"
            value="Create"
            class="btn btn-success float-right"
          />
        </div>
      </div>
    <?php echo form_close() ?>
  </section>
  <!-- /.content -->
</div>