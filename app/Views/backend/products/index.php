<?php
$errors_edit = session()->getFlashdata('errors_edit');
$errors_create = session()->getFlashdata('errors_create');
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?= $this->include('backend/templates/_section_header') ?>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="col-12">
      <!-- Error handler -->
      <?php echo view('errors/_errors_backend'); ?>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Products</h3>

              <div class="card-tools">
                <button
                  type="button"
                  class="btn btn-primary"
                  data-toggle="modal"
                  data-target="#modal-create"
                >Add Product</button>
              </div>
            </div>
            <div class="card-body">
              <table class="table table-striped table-bordered table-responsive" id="productTable">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Products</th>
                    <th>Sell Price</th>
                    <th>Buy Price</th>
                    <th>Stock</th>
                    <th>Image</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; foreach ($products as $product): ?>
                    <tr>
                      <td><?= $i ?></td>
                      <td><?= $product['name'] ?></td>
                      <td><?= number_format($product['sell_price'], 2, '.', ',') ?></td>
                      <td><?= number_format($product['buy_price'], 2, '.', ',') ?></td>
                      <td><?= $product['stock'] ?></td>
                      <td><?= img(
                        $product['img_path'],
                        FALSE,
                        ['class' => 'table-avatar']) ?>
                      </td>
                      <td><?= date('Y-m-d H:i:s', strtotime($product['created'])) ?></td>
                      <td><?= date('Y-m-d H:i:s', strtotime($product['updated'])) ?></td>
                      <td class="project-actions text-right">
                        <div class="row">
                          <button
                            id="editProduct"
                            name="product_slug"
                            type="button"
                            class="btn btn-sm btn-primary"
                            data-toggle="modal"
                            data-target="#modal-edit"
                            onclick="getProduct('<?= $product['slug'] ?>')"
                          ><i class="fas fa-pencil-alt"></i>Edit</button>
                          &nbsp;
                          <button
                            name="product_id"
                            type="button"
                            class="btn btn-sm btn-danger"
                            onclick="deleteProduct('<?= $product['id'] ?>')"
                          ><i class="fas fa-trash"></i>Delete</button>
                        </div>
                      </td>
                    </tr>
                  <?php $i++; endforeach ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-create"> <!-- modal create -->
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <?php echo form_open_multipart(
            'backend/product/create',
            'id="createProduct"'
          ) ?>
            <?= csrf_field() ?>

            <?php if (!empty($errors_create)) { ?>
              <div class="modal-header">
                <div class="container-fluid">
                  <?php echo view('errors/_errors_backend_create'); ?>
                </div>
              </div>
            <?php } ?>

            <div class="modal-header">
              <div class="modal-title">Product</div>
            </div>

            <div class="modal-body">
              <div class="form-group">
                <label for="inputName">Product Name</label>
                <input
                  type="text"
                  id="inputName"
                  name="name"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="inputName">Sell Price</label>
                <input
                  type="text"
                  id="inputName"
                  name="sell_price"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="inputName">Buy Price</label>
                <input
                  type="text"
                  id="inputName"
                  name="buy_price"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="inputName">Stock</label>
                <input
                  type="text"
                  id="inputName"
                  name="stock"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Image</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input
                      type="file"
                      class="custom-file-input"
                      name="image"
                      id="exampleInputFile"
                    />
                    <label
                      class="custom-file-label"
                      for="exampleInputFile"
                    >Choose file</label>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          <?php echo form_close() ?>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div> <!-- /.modal create -->

    <div class="modal fade" id="modal-edit"> <!-- modal edit -->
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <?php echo form_open_multipart(
            'backend/product/edit/'.$product['slug'],
            'id="editProduct"'
          ) ?>
            <?= csrf_field() ?>

            <?php if (!empty($errors_edit)) { ?>
              <div class="modal-header">
                <div class="container-fluid">
                  <?php echo view('errors/_errors_backend_edit'); ?>
                </div>
              </div>
            <?php } ?>

            <div class="modal-header">
              <div class="modal-title">Edit Product</div>
            </div>

            <div class="modal-body">
              <div class="form-group">
                <label for="edit_name">Product Name</label>
                <input
                  type="text"
                  id="edit_name"
                  name="edit_name"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="edit_sell_price">Sell Price</label>
                <input
                  type="text"
                  id="edit_sell_price"
                  name="edit_sell_price"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="edit_buy_price">Buy Price</label>
                <input
                  type="text"
                  id="edit_buy_price"
                  name="edit_buy_price"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="edit_stock">Stock</label>
                <input
                  type="text"
                  id="edit_stock"
                  name="edit_stock"
                  class="form-control"
                />
              </div>
              <div class="form-group">
                <label for="edit_image">Image</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input
                      type="file"
                      class="custom-file-input"
                      name="edit_image"
                      id="edit_image"
                    />
                    <label
                      class="custom-file-label"
                      for="edit_image"
                    >Choose file</label>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          <?php echo form_close() ?>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div> <!-- /.modal edit -->

  </section>
  <!-- /.content -->
</div>