    <?php if (uri_string() !== 'backend/login'
              && uri_string() !== 'backend/register') { ?>
      <footer class="main-footer">
        <strong>Copyright &copy; <?= date('Y') ?> <a href="<?= base_url('backend/dashboard') ?>">Test CRUD</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
          <b>Ver.</b> <?= getenv('app.version') ?>
        </div>
      </footer>
    <?php } ?>

    <!-- jQuery -->
    <?= script_tag('assets/js/jquery.min.js') ?>
    <!-- jQuery UI 1.11.4 -->
    <?= script_tag('assets/js/jquery-ui.min.js') ?>
    <!-- Bootstrap 4 -->
    <?= script_tag('assets/js/bootstrap.bundle.min.js') ?>
    <!-- overlayScrollbars -->
    <?= script_tag('assets/js/jquery.overlayScrollbars.min.js') ?>
    <!-- daterangepicker -->
    <?= script_tag('assets/js/moment.min.js') ?>
    <?= script_tag('assets/js/daterangepicker.js') ?>
    <!-- Tempusdominus Bootstrap 4 -->
    <?= script_tag('assets/js/tempusdominus-bootstrap-4.min.js') ?>
    <!-- Summernote -->
    <?= script_tag('assets/js/summernote-bs4.min.js') ?>
    <!-- BS custom file input -->
    <?= script_tag('assets/js/bs-custom-file-input.min.js') ?>
    <!-- AdminLTE App -->
    <?= script_tag('assets/js/adminlte.js') ?>
    <!-- Demo js -->
    <?= script_tag('assets/js/demo.js') ?>
    <!-- DataTables -->
    <?= script_tag('assets/js/jquery.dataTables.min.js') ?>
    <?= script_tag('assets/js/dataTables.bootstrap4.min.js') ?>
    <?= script_tag('assets/js/dataTables.responsive.min.js') ?>
    <?= script_tag('assets/js/responsive.bootstrap4.min.js') ?>

    <!-- plugin init -->
    <script>
      $(function () {
        $('#productTable').DataTable({
          "responsive": true,
          "autoWidth": false,
        });
      });
      $(function () {
        // Summernote
        $('.textarea').summernote({
          height: 200
        })
      })
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <script type="text/javascript">
      $(document).ready(function () {
        bsCustomFileInput.init();
      });

      function getProduct(slug) {
        $.ajax({
          url: `<?= base_url('backend/product/edit') ?>/${slug}`,
          type: 'GET',
          dataType: 'json',
          success: function (data) {
            $('#edit_name').val(data.name);
            $('#edit_sell_price').val(data.sell_price);
            $('#edit_buy_price').val(data.buy_price);
            $('#edit_stock').val(data.stock);
          }
        })
      }

      function deleteProduct(id) {
        if (confirm("Delete product?")) {
          $.ajax({
            url: `<?= base_url('backend/product/delete') ?>/${id}`,
            type: 'GET',
            success: function (data) {
              alert('Data has been deleted')
              window.location.href='<?= base_url('backend/products') ?>';
            }
          })
        }
      }
    </script>
  </body>
</html>