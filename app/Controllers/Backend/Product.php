<?php namespace Backend;

use App\Models\ProductModel;
use CodeIgniter\Exceptions\PageNotFoundException;
use App\Controllers\BaseController;

class Product extends BaseController
{

    /**
     * Fetch all data
     *
     * @return void
     */
    public function index()
    {
        $model = new ProductModel;
        $data['products'] = $model->getProducts();
        $data['title'] = 'Products';
        $data['content'] = view('backend/products/index', $data);
        return view($this->backend, $data);
    }

    /**
     * Get single data
     *
     * @param string $slug
     * @return void
     */
    public function view(string $slug = null)
    {
        // Create products instance
        $model = new ProductModel;

        // Get products by selected slug
        $data['products'] = $model->getProducts($slug);

        // Check if products availability
        if (empty($data['products'])) {
            throw new PageNotFoundException(
                'Cannot find the products item: '. $slug
            );
        }

        // Set page title based on product name
        $data['name'] = $data['products']['name'];

        // Display news page
        $data['content'] = view('news/view', $data);
        echo view($this->backend, $data);
    }

    /**
     * Create new entry
     *
     * @return void
     */
    public function create()
    {
        // Create news instance
        $model = new ProductModel;
        // Fetch all post data
        $data = $this->request->getPost();

        // Check if post data availability
        if (empty($data)) {
            $data['title'] = 'Add Product';

            // Display create product page
            $data['content'] = view('backend/products/create', $data);
            return view($this->backend, $data);
        } else {
            if ($this->validation->run($data, 'product') === false) {
                // Set error flash data
                $this->session->setFlashdata(
                    'errors_create',
                    $this->validation->getErrors()
                );
                // Redirect to prev route
                return redirect()->back();
            } else {
                // Get image
                $image = $this->request->getFile('image');
                // Set new name
                $file_name = $image->getRandomName();

                // Set news slug
                $data['slug'] = \url_title($data['name'], '-', true);
                // define image relative path
                $data['img_path'] = 'uploads/'.$file_name;

                // Save data
                $save = $model->save([
                    'name' => $data['name'],
                    'sell_price' => $data['sell_price'],
                    'buy_price' => $data['buy_price'],
                    'stock' => $data['stock'],
                    'img_path' => $data['img_path'],
                    'slug' => $data['slug'],
                    'created' => date('Y-m-d H:i:s'),
                    'updated' => date('Y-m-d H:i:s')
                ]);

                // Check if data has been saved
                if ($save === true) {
                    // Save image to WRITEPATH.'uploads'
                    $image->move(ROOTPATH . 'public/uploads', $file_name);

                    // Set success message
                    $this->session->setFlashdata(
                        'success',
                        'Product created successfully'
                    );

                    return redirect()->to(\site_url('backend/products'));
                } else {
                    // Fallback errors
                    $this->session->setFlashdata(
                        'error',
                        'Failed to insert data'
                    );
                    return redirect()->to(\site_url('backend/products'));
                }
            }
        }
    }

    /**
     * Edit product
     *
     * @param string $slug
     * @return void
     */
    public function editProduct(string $slug)
    {
        // Create instanace from model
        $model = new ProductModel;
        // Get all post data
        $data = $this->request->getPost();

        // Check HTTP method
        if ($this->request->getMethod() !== 'post') {
            $data['title'] = 'Edit Product';
            // Fetch news data
            $data['product'] = $model->getProducts($slug);

            echo json_encode($data['product']);
        } else {
            if ($this->validation->run($data, 'product_update') === false) {
                // Set error if errrors occured
                $this->session->setFlashdata(
                    'errors_edit',
                    $this->validation->getErrors()
                );
                // Redirect to prev route
                return redirect()->back();
            } else {
                // Get image
                $image = $this->request->getFile('edit_image');

                // Set new name
                $file_name = $image->getRandomName();

                // Set news slug
                $data['slug'] = \url_title($data['edit_name'], '-', true);
                // Define image relative path
                $data['img_path'] = 'uploads/'.$file_name;

                // Get news by ID
                $productId = $model->getProducts($slug)['id'];

                // Set all new news data
                $product = [
                    'id' => $productId,
                    'name' => $data['edit_name'],
                    'sell_price' => $data['edit_sell_price'],
                    'buy_price' => $data['edit_buy_price'],
                    'stock' => $data['edit_stock'],
                    'img_path' => $data['img_path'],
                    'slug' => $data['slug'],
                    'updated' => date('Y-m-d H:i:s')
                ];

                // Update product data
                $update = $model->updateProduct($product);

                if ($update === true) {
                    // Save image to WRITEPATH.'uploads'
                    $image->move(ROOTPATH . 'public/uploads', $file_name);

                    // Set success message
                    $this->session->setFlashdata(
                        'success',
                        'Product updated successfully'
                    );

                    return redirect()->to(\site_url('backend/products'));
                } else {
                    // Fallback errors
                    $this->session->setFlashdata(
                        'error',
                        'Failed to update data'
                    );
                    return redirect()->to(\site_url('backend/products'));
                }
            }
        }
    }

    /**
     * Delete news
     *
     * @param int $id
     * @return void
     */
    public function deleteProduct(int $id)
    {
        // Create new instance
        $model = new ProductModel;
        // Delete news by selected ID
        $query = $model->delete(['id' => $id]);

        // Check if data has been deleted
        if ($model->db->affectedRows() === 1) {
            // Set success flash data
            $this->session->setFlashdata(
                'success',
                'Product been deleted'
            );

            // Redirect to prev route
            return redirect()->to(\site_url('backend/products'));
        } else {
            // Set error flash data
            $this->session->setFlashdata(
                'error',
                'Failed to delete data'
            );

            // Redirect to prev route
            return redirect()->to(\site_url('backend/products'));
        }
    }
}
